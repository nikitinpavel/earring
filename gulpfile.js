"use strict";

//Глобальные модули
var gulp = require("gulp"),
  path = require("path"),
  data = require("gulp-data"),
  fs = require('fs'),
  pug = require("gulp-pug"),
  sass = require("gulp-sass"),
  sassGlob = require('gulp-sass-glob'),
  rename = require('gulp-rename'),
  sourcemaps = require('gulp-sourcemaps'),
  uglify = require("gulp-uglify"),
  concat = require('gulp-concat'),
  svgSprite = require('gulp-svg-sprite'),
  prefix = require("gulp-autoprefixer"),
  imagemin = require('gulp-imagemin'),
  browserSync = require("browser-sync").create(),
  file = require('gulp-file'),
  args = require('yargs').argv;

//Пути к папкам
var paths = {
  public: "./public/",
  sass: "./src/sass/",
  css: "./public/css/",
  js: "./public/js/",
  img: "./public/img/",
  svg: "./public/svg",
  data: "./src/_data/",
  blocks: "./src/blocks",
  sections: "./src/sections"
};


//Автоматическое подключение pug файлов блоков. fileName - имя файла с импортом миксинов.
function createImport(fileName) {
  var fileText = "";

  // Взять имена всех файлов в папке blocks
  fs.readdir(paths.blocks, (err, files) => {
    files.forEach(file => {
      if (file !== fileName) {
        fileText += `include ./${file}/${file}.pug` + "\n";
      }
    });
    // Создать файл импорта pug миксинов
    return file(fileName, fileText, { src: true })
      .pipe(gulp.dest(paths.blocks));
  });
}


//Создание блока

//Функция проверки наличия блока
function directoryExist(blockPath, blockName) {
  return new Promise((resolve, reject) => {
    fs.stat(blockPath, notExist => {
      if (notExist) {
        resolve();
      } else {
        reject(`ERROR>>> The file '${blockPath}' already exists.`);
      }
    });
  });
}


// Команда для запуска gulp create-block --block=имяблока
gulp.task('create-block', function (done) {
  var name = args.block; //Имя блока, задается через командную строку
  var filePug = name + '.pug';
  var fileScss = '_' + name + '.scss';
  var strScss = '';//Эта строка будет в созданом scss файле
  var strPug = `mixin ${name}()`; //Эта строка будет в созданом pug файле
  var blockFolder = path.join(paths.blocks, name)

  directoryExist(blockFolder + "/" + fileScss, args.block).then((result) => {
    return file('_' + name + '.scss', strScss, { src: true })
      .pipe(gulp.dest(blockFolder));
  }).catch((e) => {
    console.log(e);
  });

  directoryExist(blockFolder + "/" + filePug, args.block).then((result) => {
    return file(name + '.pug', strPug, { src: true })
      .pipe(gulp.dest(blockFolder));
  }).then(() => {
    createImport('includeblocks.pug'); //создать или обновить файл с импортом pug миксинов
  }).catch((e) => {
    console.log(e);
  });

  done(); //Завершение таска
});


//Компиляция pug файлов и с данными из jsnon файлов
gulp.task("pug", function () {
  return gulp
    .src("./src/*.pug")
    .pipe(
      data(function (file) {
        return JSON.parse(fs.readFileSync(paths.data + path.basename(file.path) + '.json'));
      })
    )
    .pipe(
      pug({
        pretty: true
      })
    )
    .on("error", function (err) {
      process.stderr.write(err.message + "\n");
      this.emit("end");
    })
    .pipe(gulp.dest(paths.public));
});

//Компиляция scss файлов и автопрефикс
gulp.task("sass", function () {
  return gulp
    .src(paths.sass + "main.scss")
    .pipe(sourcemaps.init())
    .pipe(sassGlob())            //глобальный импорт
    .pipe(
      sass({
        includePaths: [paths.sass],
        outputStyle: "compressed",
        sourcemap: true
      })
    )
    .on("error", sass.logError)
    .pipe(
      prefix(["last 2 versions", "> 2%", "ie 11"], {  //Настройка автопрефикса
        cascade: true
      })
    )
    .pipe(rename('main.min.css'))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.stream());
});

// Минификация и конкатенация JS кода
gulp.task("scripts", function () {
  return (
    gulp
      .src(['./src/js/main.js','./src/blocks/**/*.js'])
      .pipe(concat('main.js'))
      .pipe(uglify())
      .pipe(gulp.dest(paths.js))
  );
});



//SVG спрайты
gulp.task('svgSprite', function () {
  return gulp.src('./src/svg/*.svg')
    .pipe(svgSprite({
      mode: {
        stack: {
          sprite: "../sprite.svg"
        }
      },
    }
    ))
    .pipe(gulp.dest(paths.svg));
});

//Минификация изображений
function imgMin() {
  return gulp.src("./src/img/*")
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 4 }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { cleanupIDs: false }
        ]
      })
    ]))
    .pipe(gulp.dest(paths.img));
}

gulp.task("imgMin", imgMin);



// Отслеживание изменений в файлах
gulp.task("watch", function () {
  gulp.watch(paths.sass + "**/*.scss", gulp.series("sass"));
  gulp.watch("./src/**/*.pug", gulp.series("pug"));
  gulp.watch("./src/**/*.json", gulp.series("pug"));
  gulp.watch("./src/**/*.js", gulp.series("scripts"));
  gulp.watch("./src/img/*", gulp.series("imgMin"));
  gulp.watch("./src/svg/*", gulp.series("svgSprite"));
});

//Перезагрузка браузера
gulp.task("reload", function (done) {
  browserSync.reload();
  done();
});


gulp.task("build", gulp.series("sass", "pug", "scripts", "imgMin"));


//Запуск сервера localhost:3000, отслеживание изменений в исходных файлах с презагрузкой браузера
gulp.task("default", function () {
  browserSync.init({
    server: {
      baseDir: paths.public
    }
  });

  gulp.watch("./src/**/*pug.json", gulp.series("pug"));
  gulp.watch("./src/**/*.scss", gulp.series("sass"));
  gulp.watch("./src/**/*.pug", gulp.series("pug"));
  gulp.watch("./src/**/*.js", gulp.series("scripts"));
  gulp.watch("./src/img/*", gulp.series("imgMin"));
  gulp.watch("./src/svg/*", gulp.series("svgSprite"));
  gulp.watch("./src/**/*.*", gulp.series("reload"));
});
