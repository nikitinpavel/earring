//Счетчик обратного отсчета

function CountdownTimer(elm, tl, mes) {
  this.initialize.apply(this, arguments);
}

CountdownTimer.prototype = {

  initialize: function (elm, tl) {
    this.elem = document.getElementById(elm);
    this.tl = tl;
  },

  countDown: function () {

    var today = new Date();
    // Если время счетчика истекло, установить счетчик на один месяц
    var time = new Date();
    time.setMonth(time.getMonth() + 1);

    if ((this.tl - today) <= 0) {
      this.tl = time;
    }

    var timer = '';
    var day = Math.floor((this.tl - today) / (24 * 60 * 60 * 1000));
    var hour = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
    var min = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / (60 * 1000)) % 60;
    var sec = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / 1000) % 60 % 60;
    var me = this;

    timer += '<div class="counter__digit"><time class="counter__time counter__time--days">' + this.addZero(day) + '</time><p>дней</p></div>';
    timer += '<div class="counter__digit"><time class="counter__time">' + this.addZero(hour) + '</time><p>часов</p></div>';
    timer += '<div class="counter__digit"><time class="counter__time">' + this.addZero(min) + '</time><p>минут</p></div>';
    timer += '<div class="counter__digit"><time class="counter__time">' + this.addZero(sec) + '</time><p>секунд</p></div>';
    this.elem.innerHTML = timer;
    var tid = setTimeout(function () { me.countDown(); }, 1000);
  },

  addZero: function (num) { return ('0' + num).slice(-2); }

}
function CDT() {

  //Установить дату отсчета, значение атрибута data-time
  var counter = document.getElementById('counter');
  var timeCount = counter.getAttribute('data-time');

  var tl = new Date(timeCount);

  var timer = new CountdownTimer('counter', tl);
  timer.countDown();
}

window.onload = function () {
  CDT();
}